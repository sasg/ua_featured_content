<?php
/**
 * @file
 * ua_featured_content.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ua_featured_content_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: image
  $overrides["image.flexslider_full.effects|0|data|focal_point_advanced"] = array(
    'shift_x' => '',
    'shift_y' => '',
  );
  $overrides["image.flexslider_full.effects|0|data|width"] = 1136;
  $overrides["image.flexslider_full.effects|0|name"] = 'focal_point_scale_and_crop';
  $overrides["image.flexslider_full.effects|0|weight"] = 2;
  $overrides["image.flexslider_full.label"] = 'flexslider_full';

 return $overrides;
}
