# UA Featured Content* (Carousel) Drupal Feature Module

(*) Name subject to change.

Provides content type and view for Featured Content Carousel component consistent with UA brand strategy.

## Features

- Provides 'ua_carousel_item' content type.
- Provides carousel view for ua_carousel_item items.
- Provides 'ua_hero_carousel' Flexslider preset.

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules

- [Chaos tools](https://www.drupal.org/project/ctools)
- [Entity API](https://www.drupal.org/project/enity)
- [Features](https://www.drupal.org/project/features)
- [Flexslider](https://www.drupal.org/project/flexslider)
- [Focal point](https://www.drupal.org/project/focal_point)
- [Jquery update](https://www.drupal.org/project/flexslider)
- [Libraries](https://www.drupal.org/project/libraries)
- [Link](https://www.drupal.org/project/link)
- [Strongarm](https://www.drupal.org/project/strongarm)

### Libraries

- [Flexslider](http://www.woothemes.com/flexslider/)

### Issues

- After enabling module, go to admin/config/media/image-styles/edit/flexslider_full, override defaults and save to get focal point module working with carousel.
